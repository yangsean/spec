

Pod::Spec.new do |s|
  s.name             = "lib3"
  s.version          = "1.0.1"
  s.summary          = "lib3 a image cache library for network type."
  s.platform     = :ios, "7.0"
  s.description      = 'lib3 a image cache library for network type lib3 a image cache library for network type lib3 a image cache library for network type'

  s.homepage         = "https://bitbucket.org/yangsean/lib3"
  s.license          = 'MIT'
  s.author           = { "yanghonglei" => "272789124@qq.com" }
  s.source           = { :git => "https://yangsean@bitbucket.org/yangsean/lib3.git", :tag => s.version.to_s }
  #s.ios.deployment_target = '7.0'

  s.source_files = 'lib3/Classes/*.{h,c,m,swift}'
  s.resources = 'lib3/Classes/*.{xib,nib,plist}'
  s.requires_arc = true
end